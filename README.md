## What i did until now?
### Set our robot as package in the robots folder of robocode
    * run `java -jar robocode.a.b.c.d.jar` a.b.c.d being version
    * robocode folder is the one where the jar was installed
    * create project on IntelliJ adding robocode/robots as root folder
    * delete the automatic created 'src' folder and set the others folders as excluded from the project structure
    * mark the robocode/robots as sources root 
    * create the folder of our robot (here robot_ai_mata64)
    * set this folder as *package*
    * create our java class main inside robot_ai_mata64

### Adjusting project structure
    * go on File -> Project Structure
    * lets set the robocode itself as compiler in Modules -> Paths
    * click on use module compile output path and set to 'robocode/robots' in Output and 'robocode/robots/robots/' in Test
    * click on 'Exclude output paths'
    * now lets add the libs in the Libraries tab on left
    * in there, click on the plus icon and the in Java (of course)
    * then, select libs folder in the robocode folder and select all the .jar files there (except the annotations) and set as libs
    * now, going on project structure again, it detects problems
    * click on Fix to add the Robocode itself as dependencie
    * now we're done!

### Add and compiling robot
    * In the robocode, go on Options -> Development Options
    * add the path of the folder and enable it
    * now, open the editor of robocode (Robot -> Editor)
    * open the java file of our robot and try to compile it
    * now we're done! The robot is available on the list of robots.