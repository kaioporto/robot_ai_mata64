package robot_ai_mata64;

//import robocode.AdvancedRobot;
import robocode.Robot;
import robocode.HitByBulletEvent;
import robocode.HitWallEvent;
import robocode.ScannedRobotEvent;

import java.awt.*;

public class minRobot extends Robot {


    public void onScannedRobot (ScannedRobotEvent e){
        double radar_position = getRadarHeading();
        String target_robot = e.getName();
        double target_bering = e.getBearing();
        double target_speed = e.getVelocity();
        double target_distance = e.getDistance();

        if(target_distance > 150){
            fire(1);
        }
    }



    public void onHitWall(HitWallEvent e){
        // scan direction to get away from the wall
        // walk
    }

    public void onHitByBullet(HitByBulletEvent e){
        // start dodge function
    }

    public void safeDistance(double robotDistance){

        if (robotDistance < 50)
            fire(3);
        else if(robotDistance > 150)
            fire(1);
    }

    public void dodgeCrazy(){

    }

    
    public void run(){
        setColors(Color.BLACK, Color.BLUE, Color.RED);
        while(true){
            // movimentation here. naive example below
            turnRadarLeft(360);
            ahead(300);
        }
    }

}
